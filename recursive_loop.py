import json

# read file
with open('data.json', 'r') as my_file:
    data = my_file.read()

# parse file
my_dict = json.loads(data)
find = input('string to find: ')


def dict_path(path, my_dict, find, guid=None):

    for k, v in my_dict.items():
        if isinstance(v, dict):
            dict_path(path, v, find, guid)
        else:
            if isinstance(v, list):
                for vv in v:
                    if isinstance(vv, dict):
                        dict_path(path, vv, find, guid)
            else:
                if isinstance(v, str) and find in v.lower():
                    print(path+'_ID_'+guid, "=>", v, '\n\r\n')
                else:
                    if k == 'title':
                        path = path+'_'+v
                    if k == 'guid':
                        guid = v


dict_path("", my_dict, find)
