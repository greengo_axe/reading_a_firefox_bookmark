# Reading a Firefox's bookmark

## Abstract 
It can iterate through the bookmark file (JSON format) and found a given string. The script prints both values found and related paths, if any. If not, nothing.

## Examples

**With results:**

    $ python recursive_loop.py 

        string to find: company
        __Bookmarks Toolbar_applications_sent_2020_dec_app1_ID_s7kZywHnWpYj => https://company1.com/ 


        __Bookmarks Toolbar_applications_sent_2020_dec_app2_ID_9kh1a8tFvFzY => https://www.company2.com/ 
        
        
        __Bookmarks Toolbar_applications_sent_2021_apr_ID_a1dw37qdEg3M => company55 
        
        
        __Bookmarks Toolbar_applications_sent_2021_apr_ID_a1dw37qdEg3M => https://company55.com/ 
        
**Without results**

    $ python recursive_loop.py 

        string to find: hjdasjdhasihdas

    $


## Premise
This project requires :

* Python 3.8

It has to be up and running. 


## Installation
* The root folder of the project is 
 
    >**reading_a_firefox_bookmark** 
    
* and the absolute path is
 
    >**/var/www/html/reading_a_firefox_bookmark**
    
* Clone the project from gitlab on your Desktop :
    
    >**git@gitlab.com:greengo_axe/reading_a_firefox_bookmark.git**

* copy and overwrite all folders and files from 

>>**Desktop/reading_a_firefox_bookmark**

>into 

>>**/var/www/html/reading_a_firefox_bookmark**

* Delete the folder on your Desktop (**Desktop/reading_a_firefox_bookmark**)


* Enjoy!


## F.A.Q

## Troubleshooting
 
## TODO List:
 * Whatever you like
